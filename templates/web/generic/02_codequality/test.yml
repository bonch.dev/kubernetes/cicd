.test-defaults-write: &test-defaults-write
  - |
     echo -e "\e[0Ksection_start:`date +%s`:test-defaults[collapsed=true]\r\e[0KWrite defaults for tests"
     echo '' > ./test-report.out
     echo '' > ./test-report.junit.xml
     echo '' > ./test-coverage.out
     echo '' > ./test-coverage.cobertura.xml
     echo -e "\e[0Ksection_end:`date +%s`:test-defaults\r\e[0K"

.test-template:
  stage: CodeQuality
  tags:
    - kubernetes
  allow_failure: true
  variables:
    APP_ENV: test
    TESTS_SERVICED: "false"

    WELCOME_TEST_MESSAGE: |
      Welcome to testing. 
      Please, check, that your tests writing correctly this files:
      * test-report.out             - for sonarqube reports 
      * test-coverage.out           - for sonarqube coverage reports  
      * test-report.junit.xml       - for gitlab reports
      * test-coverage.cobertura.xml - for gitlab coverage reports

    UNSERVICED_TEST_MESSAGE: |
      This is unserviced test. No services is available. 
      Please, use mocks for testing or switch to serviced "extends: .test-suite.serviced" in your test job.

    SERVICED_TEST_MESSAGE: |
      This is serviced test. Services enabled:
      * postgres
      * redis
      * rabbitmq
      * meilisearch

  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_COMMIT_TITLE =~ /^hot-fix.*/'
      when: never
    - if: '$TEST_DISABLED == "true"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_REF_PROTECTED == "true" || $CI_COMMIT_BRANCH == "master"'
  before_script:
    - |
      cat << EOF 
      $WELCOME_TEST_MESSAGE
      ‎ ‎ ‎
      $UNSERVICED_TEST_MESSAGE
      EOF
    - *test-defaults-write

.test-suite:
  extends: .test-template
  artifacts:
    when: always
    paths:
      - test-report.out
      - test-report.junit.xml
      - test-coverage.out
      - test-coverage.cobertura.xml
    reports:
      junit: test-report.junit.xml
      codequality: test-coverage.out
      coverage_report:
        coverage_format: cobertura
        path: test-coverage.cobertura.xml

.test-suite.report-only:
  extends: .test-template
  artifacts:
    when: always
    paths:
      - test-report.out
      - test-report.junit.xml
    reports:
      junit: test-report.junit.xml

.test-suite.coverage-only:
  extends: .test-template
  artifacts:
    when: always
    paths:
      - test-coverage.out
      - test-coverage.cobertura.xml
    reports:
      codequality: test-coverage.out
      coverage_report:
        coverage_format: cobertura
        path: test-coverage.cobertura.xml

.test-template.serviced:
  extends: .test-template
  variables:
    TESTS_SERVICED: "true"
    ### Postgres Service Variables
    POSTGRES_USER: db
    POSTGRES_PASSWORD: db
    POSTGRES_DB: db
    DATABASE_URL: postgres://db:db@postgres:5432/db

    ### RabbitMQ service Variables
    RABBITMQ_USERNAME: user
    RABBITMQ_PASSWORD: rabbitmq

    ### Redis Service Variables
    REDIS_PASSWORD: redis

    ### Meilisearch Service Variables
    MEILI_ENV: "development"
    MEILI_NO_ANALYTICS: "true"
    MEILI_MASTER_KEY: "meilisearch"
  services:
    - name: bitnami/postgresql:14.9.0
      alias: postgres

    - name: bitnami/redis:7.0.13
      alias: redis

    - name: bitnami/rabbitmq:3.12.6
      alias: rabbitmq

    - name: getmeili/meilisearch:v1.4.1
      alias: meilisearch
  before_script:
    - |
      cat << EOF 
      $WELCOME_TEST_MESSAGE
      ‎ ‎ ‎
      $SERVICED_TEST_MESSAGE
      EOF
    - *test-defaults-write

.test-suite.serviced:
  extends: .test-template.serviced
  artifacts:
    when: always
    paths:
      - test-report.out
      - test-report.junit.xml
      - test-coverage.out
      - test-coverage.cobertura.xml
    reports:
      junit: test-report.junit.xml
      codequality: test-coverage.out
      coverage_report:
        coverage_format: cobertura
        path: test-coverage.cobertura.xml

.test-suite.serviced.report-only:
  extends: .test-template.serviced
  artifacts:
    when: always
    paths:
      - test-report.out
      - test-report.junit.xml
    reports:
      junit: test-report.junit.xml

.test-suite.serviced.coverage-only:
  extends: .test-template.serviced
  artifacts:
    when: always
    paths:
      - test-coverage.out
      - test-coverage.cobertura.xml
    reports:
      codequality: test-coverage.out
      coverage_report:
        coverage_format: cobertura
        path: test-coverage.cobertura.xml

Test:
  extends: .test-suite
  variables:
    EMPTY_TEST_MESSAGE: |
      This is empty test job.
      If you want to add tests for your projects, you need to add job for your .gitlab-ci.yml
      Example of Test job:
      ‎ ‎ ‎ 
      Test:
        image: alpine:latest 
      #   Some custom image, like alpine:3.15
      #   For Laravel projects, you must use $$BUILD_IMAGE variable, like:
      #   image: $$BUILD_IMAGE
        extends: .test-suite
        script:
          - echo 'run my beautiful test' > ./test-report.out
      ‎ ‎ ‎ 
      Or serviced Test job:
      ‎ ‎ ‎ 
      Test:
        image: alpine:latest 
        extends: .test-suite.serviced
        script:
          - echo 'run my beautiful test' > ./test-report.out
      ‎ ‎ ‎
      See more examples below <Colapsed> sections
    EXAMPLE_GOLANG: |
      Test:
        image: golang:1.21-alpine
        extends: .test-suite.serviced.report-only
        variables:
          APP_ENV: test
          CGO_ENABLED: "0"
        script:
          - go install github.com/jstemmer/go-junit-report/v2@latest
          - go run ./cmd/migration
          - |
            go test -timeout 60s -p 1 ./... -json 2>&1 | \
              tee test-report.out | \
              go-junit-report -parser gojson -set-exit-code > test-report.junit.xml
      
      Test Coverage:
        image: golang:1.21-alpine
        extends: .test-suite.serviced.coverage-only
        variables:
          APP_ENV: test
          CGO_ENABLED: "0"
        script:
          - go install github.com/boumenot/gocover-cobertura@latest
          - go run ./cmd/migration
          - |
            go test -timeout 60s -p 1 \
              -cover \
              -coverprofile=test-coverage.out \
              -covermode=count ./...
          - gocover-cobertura < test-coverage.out > test-coverage.cobertura.xml
    EXAMPLE_LARAVEL_PEST: |
      Test:
        image: 
          name: $$BUILD_IMAGE
          entrypoint: [""]
        extends: .test-suite.laravel.serviced
        variables:
          APP_ENV: test
        script:
          - php artisan migrate && php artisan db:seed
          - |
            php -d zend_extension=xdebug.so ./vendor/bin/pest \
                 --coverage-clover=$$CI_PROJECT_DIR/test-coverage.out \
                 --coverage-cobertura=$$CI_PROJECT_DIR/test-coverage.cobertura.xml \
                 --log-junit=$$CI_PROJECT_DIR/test-report.junit.xml \
                ./tests/
    EXAMPLE_LARAVEL_PHPUNIT: |
      Test:
        image:
          name: $$BUILD_IMAGE
          entrypoint: [""]
        extends: .test-suite.laravel.serviced
        variables:
          APP_ENV: test
        script:
          - php artisan migrate && php artisan db:seed
          - |
            php -d zend_extension=xdebug.so ./vendor/bin/phpunit \
                --coverage-clover=$$CI_PROJECT_DIR/test-coverage.out \
                --coverage-cobertura=$$CI_PROJECT_DIR/test-coverage.cobertura.xml \
                --log-junit=$$CI_PROJECT_DIR/test-report.junit.xml \
                ./tests/
    EXAMPLE_LARAVEL_XDEBUG: |
      FROM alpine:3.19

      # Install packages and remove default server definition
      RUN apk --no-cache add  \
      ... php83-pecl-imagick php83-xdebug \
      ... && \
      echo 'zend_extension=xdebug.so' >> /etc/php83/conf.d/50_xdebug.ini && \
      ...

  script:
    - |
      cat << EOF 
      $EMPTY_TEST_MESSAGE
      EOF
    - |
      echo "Golang test example"
      echo -e "\e[0Ksection_start:`date +%s`:example-golang[collapsed=true]\r\e[0K<Collapsed>"
      cat << EOF
      $EXAMPLE_GOLANG
      EOF
      echo -e "\e[0Ksection_end:`date +%s`:example-golang\r\e[0K"
    - |
      echo "Laravel notice: you must include the xdebug extension in your Dockerfile for coverage report"
      echo -e "\e[0Ksection_start:`date +%s`:example-laravel-xdebug[collapsed=true]\r\e[0K<Collapsed>"
      cat << EOF
      $EXAMPLE_LARAVEL_XDEBUG
      EOF
      echo -e "\e[0Ksection_end:`date +%s`:example-laravel-xdebug\r\e[0K"
    - | 
      echo "Laravel pest example"
      echo -e "\e[0Ksection_start:`date +%s`:example-laravel-pest[collapsed=true]\r\e[0K<Collapsed>"
      cat << EOF
      $EXAMPLE_LARAVEL_PEST
      EOF
      echo -e "\e[0Ksection_end:`date +%s`:example-laravel-pest\r\e[0K"
    - |
      echo "Laravel phpunit example"
      echo -e "\e[0Ksection_start:`date +%s`:example-laravel-phpunit[collapsed=true]\r\e[0K<Collapsed>"
      cat << EOF
      $EXAMPLE_LARAVEL_PHPUNIT
      EOF
      echo -e "\e[0Ksection_end:`date +%s`:example-laravel-phpunit\r\e[0K"
